#use "scheme.ml";;
#use "testUtils.ml";;

let rec unparse_list2 = function
    [] -> ""
  | (x::[]) -> unparse2 x
  | (x::xs) -> (unparse2 x) ^ ";" ^ (unparse_list2 xs)

and unparse2 = function
  | Id id -> "(Id \"" ^ id ^ "\")"
  | Num n -> "(Num " ^ string_of_int n ^ ")"
  | Bool true -> "(Bool true)"
  | Bool false -> "(Bool false)"
  | String s -> "(String \"" ^ s ^ "\")"
  | List l -> "(List [" ^ unparse_list2 l ^ "])"
;;

let test_parse x = List.map (fun y -> 
	print_endline ("% " ^ y) ;
	print_endline (unparse2 (parse (tokenize y))))
	x
;;
let test_eval x = List.map (fun y -> 
	print_endline ("% " ^ (unparse y));
	print_endline (string_of_value (eval y))) x
;;

(* Your test cases here *)
(*
test_parse [ "(1 (2 (3 ( 4 ( 5 ) ) ) 6) )" ] ;;
*)
test_eval [ (List [(Id "define");(Id "x");(Num 2)]) ; ] ;;
test_eval [Id "nil"] ;;
test_eval [Id "x"] ;;
test_eval [ (List [(List [(Id "lambda");List[(Id "x")];List [(Id "+");(Id "x");(Num 2)]]); (Num 2)])] ;;

test_eval [(List [(Id "define");(Id "x");(Num 52)]) ;
	(List [(Id "define");(Id "foo");
		(List [(Id "lambda");
		(List [(Id "x")]);
		(List [(Id "lambda");
		(List [(Id "y")]);
		(List [(Id "+");(Id "x");(Id "y")])])])]) ;
	(List [(List [(Id "foo");(Num 3)]);(Num 4)]) ;
	(List [(Id "define");(Id "bar");
		(List [(Id "lambda");
		(List [(Id "y")]);
		(List [(Id "+");(Id "x");(Id "y")])])]) ;
	(List [(Id "bar");(Num 2)]) ;
	(List [(Id "define");(Id "x");(Num 7)]) ;
	(List [(Id "bar");(Num 2)]) ] ;;

test_eval [(List [(Id "define");(Id "nested");
		(List [(Id "lambda");
		(List [(Id "x")]);
		(List [(Id "lambda");
		(List [(Id "x")]);
		(List [(Id "+");(Id "x");(Id "x")])])])])];;

test_eval [(List[(List[(Id "nested");(Num 1)]);(Num 3)])]
